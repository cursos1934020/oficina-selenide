package org.pantoja.oficinaSelenide.unitTestExample;

public class Calculadora {

    public Double soma(Double... numeros){
        Double total = 0.0;
        for(Double numero : numeros){
            total+=numero;
        }

        return total;
    }

    public Double subtracao(Double... numeros){
        Double total = numeros[0];
        int interacao = 0;
        for(Double numero : numeros){
            if(interacao == 0) {
                interacao++;
                continue;
            }
            total-=numero;
        }

        return total;
    }

    public Double divisao(Double... numeros){
        Double total = numeros[0];
        int interacao = 0;
        for(Double numero : numeros){
            if(interacao == 0) {
                interacao++;
                continue;
            }
            total/=numero;
        }

        return total;
    }

    public Double multiplicacao(Double... numeros){
        Double total = numeros[0];
        int interacao = 0;
        for(Double numero : numeros){
            if(interacao == 0) {
                interacao++;
                continue;
            }
            total*=numero;
        }

        return total;
    }

}
