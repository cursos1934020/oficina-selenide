package org.pantoja.oficinaSelenide.unitTestExample;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CalculadoraTest {


    @Test
    void testSomaCalculadora(){
        Calculadora calculadora = new Calculadora();

        Double totalEsperado = 10.5 + 20.4 + 30.5 + 50.3;
        Double totalObtido = calculadora.soma(10.5, 20.4, 30.5, 50.3);

        assertEquals(totalEsperado, totalObtido);
    }


    @Test
    void testSubtracaoCalculadora(){
        Calculadora calculadora = new Calculadora();

        Double totalEsperado = 5.4 - 2.1;
        Double totalObtido = calculadora.subtracao(5.4, 2.1);

        assertEquals(totalEsperado, totalObtido);
    }

    @Test
    void testDivisaoCalculadora(){
        Calculadora calculadora = new Calculadora();

        Double totalEsperado = 9.0/3.0/3.0;
        Double totalObtido = calculadora.divisao(9.0,3.0,3.0);

        assertEquals(totalEsperado, totalObtido);
    }

    @Test
    void testMultiplicacaoCalculadora(){
        Calculadora calculadora = new Calculadora();

        Double totalEsperado = 9.0*3.0*3.0;
        Double totalObtido = calculadora.multiplicacao(9.0,3.0,3.0);

        assertEquals(totalEsperado, totalObtido);
    }
}
