## Introdução. max: 10min

1. O que é Teste de software.
2. O que são os testes automatizados.
3. Quais os tipos de teste automatizado.
4. Piramide de testes.

## Conceitos de desenvolvimento de testes. max: 10min

1. Junit5.
2. Tests Smells.
3. Boas práticas no desenvolvimento de testes.
4. TDD - Desenvolvimento orientado a testes
5. BDD - Desenvolvimento orientado a comportamento

## Conceitos de desenvolvimento de testes web. max: 1h

1. Criando o primeiro test web.
2. Padrão fluent page object.
3. Criando seletores com xpath.
4. Desenvolvendo testes de registro.
5. Desenvolvendo testes de login.
6. Executar testes no modo headless.
7. Tirar print dos testes.

## Report de testes. max: 10min

1. Adicionando allure report aos testes
2. Adicionando steps ao allure report.
3. Adicionando print aos steps do allure report.
4. Gerar allure report.